hexo.extend.tag.register('link_for', function(args, content) {
    var url = args[0];
    var text = args[1];
    var urlOffset = hexo.config.root || '/';
    return `<a href="${urlOffset}${url}">${text}</a>`.replace('//', '/');
});
