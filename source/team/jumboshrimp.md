---
streamer: Jumboshrimp
streamer_id: jumboshrimp
picture: https://files.mastodon.social/cache/accounts/avatars/001/412/466/original/ca74eadc210f3d87.jpg
country: 🏴󠁧󠁢󠁳󠁣󠁴󠁿
tags:
    - Project Lead
    - Streamer
    - Tech
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Placerat duis ultricies lacus sed turpis tincidunt id aliquet risus. 

<!-- more -->

# Jumboshrimp

<img src="https://files.mastodon.social/cache/accounts/avatars/001/412/466/original/ca74eadc210f3d87.jpg" height="128" width="128" style="margin: 10px auto; display: block;" alt="jumboshrimp"/>

| Facts        |              |
|:------------:|:------------:|
| Country      | Scotland 🏴󠁧󠁢󠁳󠁣󠁴󠁿  |
| Pronouns     | he/him       |
| Joined Jnktn | 2020         |

## Intro

Tune in as jumboshrimp brings ashore his picks of the week. If you're into quality and variety, look no further, since he'll make sure to sort you out with a feast for the ears. Whether he sticks to a genre or mixes it up, it's always set to be a adventure. What will be his 'tuna the day'? Only one way to find out!

## Shows

{% mixcloud_playlist jumboshrimps-radio-show 4 %}
{% mixcloud_playlist jumboshrimp 1 %}

## Get in touch

- [Mastodon](https://mastodon.social/web/@jumboshrimp@fosstodon.org)