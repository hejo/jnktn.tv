---
streamer: Ruffy
streamer_id: ruffy
picture: https://files.mastodon.social/accounts/avatars/000/394/214/original/667b3703d6b69d11.jpg
country: 🇦🇹
tags:
    - Streamer
    - DJ
---

Dancing in the moonlight. Everybody's feeling warm and bright. It's such a fine and natural sight. Everybody's dancing in the moonlight

<!-- more -->

# Ruffy

| Facts        |             |
|:------------:|:-----------:|
| From         | Linz, AT 🇦🇹 |
| Pronouns     | he/him      |
| Joined Jnktn | 2021        |
| Cats/Dogs    | Woof Woof   |

## About me

Hey stranger, happy you managed to reach my Jnktn page. My name is ruffy (pronounced like: "roughy") and I'm a music-producing software-developer. It's like you'd put a programmer and a DJ into a blender, blend it for like 20 minutes and whatever comes out, is most likely called ruffy. Also a huge fan, supporter and enthusiast of F(L)OSS projects and pretty much all kinds of technology. Aside from all this tech stuff, I'm producing music, mostly electronic dance music. Here on Jnktn I'm a resident DJ with a somewhat regular show called [Dance Attack](/shows/danceattackwithruffy). Sometimes I help out a little bit behind the scenes. 


## My favorite Jnktn moments

{% mixcloud https://www.mixcloud.com/Jnktn_TV/veras-radio-show-03-07-21/ %}

{% mixcloud https://www.mixcloud.com/Jnktn_TV/unpopular-stream-10-07-21/ %}

{% mixcloud https://www.mixcloud.com/Jnktn_TV/get-ready-with-andrina-28-05-22/ %}


## My world in tags

![ruffy wordcloud](https://blog.rtrace.io/images/wordcloud.svg)


## Latest work 

{% soundcloud track 1221158164 %}
{% soundcloud track 1098710812 %}
{% soundcloud track 1099278472 %}


## Latest Jnktn Shows

{% mixcloud_playlist dance-attack-with-ruffy 2 %}

## Get in touch

- [Mastodon](https://mastodon.social/web/@rarepublic)
- [Ruffys Blog](https://blog.rtrace.io)
- [Soundcloud](https://soundcloud.com/djraremusic)