#!/usr/bin/env sh

# possible values: 'podman' or 'docker'
oci_tool='podman'

# possible values: 'nginx' or 'caddy'
webserver_type='nginx'

identify_commit_hash()
{
    if [ -z "$CI_COMMIT_SHA" ]; then
        # this variable is exposed by Gitlab CI
        echo "$CI_COMMIT_SHA"
    fi;

    # fallback to local git
    git rev-parse HEAD
}

identify_commit_branch()
{
    if [ -z "$CI_COMMIT_BRANCH" ]; then
        # this variable is exposed by Gitlab CI
        echo "$CI_COMMIT_BRANCH"
    fi;

    # fallback to local git
    git rev-parse --abbrev-ref HEAD
}

commit_hash=$(identify_commit_hash)
commit_branch=$(identify_commit_branch)

$oci_tool build \
    -t "jnktn" \
    -t "jnktntv$webserver_type" \
    --build-arg COMMIT_HASH=$commit_hash \
    --build-arg COMMIT_BRANCH=$commit_branch \
    -f ../Dockerfile.$webserver_type