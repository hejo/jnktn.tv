#!/usr/bin/env sh

/kaniko/executor \
    --context $CI_WORKSPACE \
    --dockerfile $CI_WORKSPACE/Dockerfile.caddy \
    --destination ttl.sh/$CI_COMMIT_SHA:1h \
    --reproducible \
    -v debug